# ROSANNA/UNIMIB/PhD/AI - Activities Logbook 



## Causal Network/AI


#### 2021/09/27

- Causal Network lesson


#### 2021/10/04

- P. Lucas lesson on causal network

#### 2021/10/15

- F. Stella AI lesson to INT

#### 2021/10/22, 2021/11/04, 2021/11/11-2021/11/12, 2021/11/19, 2021/12/02

- Preparation to the exam (study and presentation)

#### 2021/11/03, 2021/11/24

- Meeting with A. Zanga to prepare the exam 

## MAD-LAB meetings

#### 2021/10/04

- A. Bregoli presents his research activity on Chronic Kidney Disease 

#### 2021/10/12

- Afternoon at UNIMIB with MADLAB team

#### 2021/10/25-2021/10/29

- Preparation of my MADLAB presentation "Causal inference and real-world data: behind the scenes"

#### 2021/11/01

- My presentation to MADLAB

#### 2021/12/01

- A. Zanga presents his work on Causal discovery  

#### 2022/01/20

- E. Cavenaghi presents his research activity on Reinforcement Learning

#### 2022/02/04

- Carlos Villa Blanco presents his research activity


## ROSANNA

### 2021/09/27

- Literature review for recurrence definition algorithms


### 2021/10/06

- WP1 informal meeting to discuss INT data collection


### 2021/10/07

- ROSANNA meeting at INT: WP2 results and other WPs updates

### 2021/10/20-2021/10/22

- ADA DB: Patient's treatment definition

### 2021/10/21

- WP1 informal meeting to discuss INT data collection

### 2021/11/22-2021/11/26

- Revision of patient's treatment definition in ADA DB: 2009-2015 Patient's, with pharmas

### 2021/11/24

- Planning for 2022 with F. Stella

### 2021/12/01

- Discussion about how to start the analysis, DAG and planning for 2022 with A. Trama

### 2021/12/14

- Discussion about the next steps planning with F. Stella: proritize literature review and model defintion

### 2021/12/15

- Brief updates with all WPs involved: WP2 first results and WP3 review

### 2021/12/20

- Regular updates with A. Trama: proritize treatment definition 

### 2021/12/31, 2022/01/02-2022/01/05, 2022/02/01-02

- Literature review for "dynamic+modeling+disease+progression"

### 2022/01/02-2022/01/08

- First DAG definition

### 2022/01/11

- Updates about litterature review and DAG definition with F. Stella. 
-->Next on: 1)discuss with A. Trama about the DAG and decide how to proceed with variables definition.  
            2) Extend litterature review to the references included in the paper by Van Der Shaar et al.
Next update: 2022/01/24-->moved to 2022/02/07

### 2022/01/11

- Treatment definition in the Ada cohort, discussion with A. Trama: we decided to start from treatment definition.
Updates will be regular based on results, deadline March 2022 for treatment definition

### 2022/01/28-31
- Ada cohort treatment definition

### 2022/01/31
- Discussion with A. Trama and P. Baili about variables collection in the INT cohort: variables selected, the collection will start from next week

### 2022/02/07

- Updates about litterature review and analysis with F. Stella. 
-->Next on: 1) Tretment variables definition in the population-based DB
            2) Review: still 44 articles from Bica et al.
Next update: 2022/02/21-->Changed into updates on Sarcomas project

### 2022/02/24-25, 2022/03/01-04, 2022/03/21-23, 2022/03/28-30
- Ada cohort treatment definition

### 2022/03/24-25, 2022/03/31, 2022/03/01
- Litterature review


## SARCOMAS&UNIMIB

#### 2021/09/29

- Meeting with sarcomas experts at INT-Campus Cascina Rosa

#### 2021/10/11

- Discussion with F. Stella on the project

#### 2021/10/19

- Call with A. Zanga to organize the work
- Definition of a world file with questions for experts about the STREXIT variables

#### 2021/10/20, 2021/11/17-2021/11/18

- Definition of a world file with questions for experts about the STREXIT variables

#### 2022/01/13

- Call with A. Zanga to organize the work

#### 2022/01/19

- Call with the experts

#### 2022/02/03-05

- Preparation for next call (07/02/2022): Call with D. Callegaro, Updates with A. Zanga and new DAG preparation

#### 2022/02/07

- Call with the experts: discussion on the DAG and on the analysis beginning

#### 2022/02/07

- Discussion with F. Stella and A. Zanga about the analysis and next meeting scheduling: 2022/03/02

#### 2022/02/08

- Exploratory analysis and data discovery with A. Zanga at UNIMIB

#### 2022/02/11, 2022/02/14-15

- Analisys on STREXIT DB

#### 2022/02/21

- Updates with A.Zanga and F. Stella: how to proceed with the analysis

#### 2022/02/22

- Meeting with A. Zanga and D. Callegaro about the analysis

#### 2022/02/25, 2022/02/28, 2022/03/01

- Preparation for next meeting

### 2022/03/02

- Call with the experts: black&white lists and tiers definitions

### 2022/03/25

- Discussion of the results with A. Zanga and F. Stella: planning for next meeting


## PGM-2022

#### 2022/01/10

- Call with F. Stella and A. Zanga. Chosen topic: trasportability!

#### 2022/01/14-2022/01/15, 2022-01/29-30

- Litterature review on trasportability


## Miscellaneous

### SMARTCARE

#### 2021/10/28-2021/10/29

- Paper revision

#### 2021/11/29

- Workshop

### PhD: UNIMIB activities

#### 2021/11/03

- PhD Course presentation at U14-UNIMIB

#### 2021/11/24

- Computer science courses presentation
- Meeting with the Tutor

### Interdisciplinar courses 

#### 2021/11/24, 2021/12/01, 2021/12/15, 2021/12/22

- Interdisciplinar course: "Productivity tools for young researchers"

#### 2021/12/28-2021/12/30, 2022/01/16-2022/01/18

- Working on final assignment for interdisciplinar course: "Productivity tools for young researchers"

#### 2022/01/19, 2022/01/20

- Interdisciplinar course: "Surfing the academic job market"

### Computer science courses

#### 2022/03/25, 2022/03/31, 2022/04/01

- Computer science course: "The many facets of uncertainty in computer science"
