#  NEXT ON:

### (ROSANNA) 
- Treatment definition: work in progress with regular updates with A. Trama 
- Litterature review deadlines:
        -End of May: litterature collection and reading completed
        -End of July: review draft
        -End of September/October: paper submission 

### (PhD) 
- Computer science course on "The many facets of uncertainty in computer science": lessons and final assignement

### (PGM2022) 
- Work with A. Zanga about Causal discovery from different databases


### (MEETINGS OUTSIDE ITALY) 
- Oslo meeting with Norwegian CR May 8th-10th
- GRELL Meeting-Pamplona May 18th-20th
