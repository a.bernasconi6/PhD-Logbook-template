# Roadmap

!! UNDER MAINTENANCE!!


```mermaid
gantt
dateFormat  YYYY-MM-DD
title ROSANNA activities
excludes weekdays 2021-09-25

section PhD 
Models review              :active,  des1, 2021-09-27, 120d
Future task               :des2, after des1, 60d

section DB 
Recurrence               :active,  des3, 2021-09-27, 30d
Treatment(//CARE)               :active,  des4, 2021-09-27, 30d

section Deadlines
ENCR poster               :des5, 2021-11-16, 3d
Peter Lucas lesson               :des6, 2021-10-14, 3d

```
```mermaid
gantt
dateFormat  YYYY-MM-DD
title Other INT activities
excludes weekdays 2021-09-25

section IPAAC Collaborative paper
Review              :active,  des7, 2021-09-27, 30d
Workplan              :des8, after  des7, 15d

section Frontiers-Ada 
Paper Finalization               :  des9, 2021-09-27, 60d

section Germ cell tumors
Paper Finalization               : des10, 2021-09-27, 60d

section Deadlines
INT seminar               :des11, 2021-10-18, 1d
```



