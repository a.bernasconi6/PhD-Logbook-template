# INT - Activities Logbook 


## CARE

#### 2021/09/27

- Breast cancer treatment codes revision according to Russo et al. (usefull for ROSANNA too)

#### 2021/09/30

- Face to face discussion for Breast cancer treatment definition in administrative databases (with S. Di Cosimo and R. Miceli)

#### 2021/10/05

- Comorbidity score discussion with P. Lasalvia

#### 2021/11/08-2021/11/09

- Support to the preparation of reserch department presentation about the project

#### 2021/11/15

- Reserch department presentation about the project

#### 2021/11/16-2021/11/17, 2021/12/02, 2022/01/11

- Abstract for ESMO 2022 preparation and discussion

#### 2021/11/15, 2021/11/22, 2021/12/03, 2021/12/10, 2021/12/20, 2022/01/26

- Discussion and work for paper on cancer incidence 2019-2020

#### 2021/12/24-2021/12/27, 2022/01/21, 2022/02/03-04

- Paper on cancer incidence writing

#### 2022/01/14, 2022/01/17, 2022/01/26, 2022/02/08

- Paper on cancer incidence discussion with A. Trama and P. Lasalvia

#### 2022/01/17, 2022/01/22

- Comorbidity score discussion with A. Trama and P. Lasalvia

#### 2022/01/17

- Presentation of databases to S. Ljevar and L. Porcu

#### 2022/01/20

- Call with F. Franchi-UNIMIB about the paper on comorbdity score.

#### 2022/01/21, 2022/01/24-28, 2022/01/31, 2022/02/01-03, 2022/02/09

- Paper on cancer incidence and comorbidity score further analysis

#### 2022/02/15-17, 2022/02/21

- Paper on cancer incidence: revision and submission

## iPAAC

#### 2021/09/15

- iPAAC workshop (11am-17pm)

#### 2021/09/28

- Discussion with Norway for pilot data collection- (trip to Norway next November?)


## PANCARE-Paper on late mortality (with M. Terenziani)

#### 2021/10/14

- Paper face-to-face discussion with A. Trama and L. Botta

#### 2021/10/17-2021/10/18

- Methods & results revision

#### 2021/12/14-2021/12/16, 2021/12/22-2021/12/23, 2022/01/10-2022/01/11, 2022/01/20, 2022/01/23-24, 2022/02/08-09

- Paper revision & submission: JNCI, EJC, Cancer



## Ada

### FRONTIERS-Paper on burden of late effects in AYA with hematological cancer (with C. Vener)

#### 2021/09/30

- Discussion with C. Vener about paper results

#### 2021/10/18

- C. Vener comments revision

#### 2021/10/21-2021/10/22, 2021/11/23

- Paper revision for submission

#### 2021/12/20-2021/12/23, 2022/01/05

- Answers to reviewers and new submision

#### 2022/02/03-2022/02/04, 2022/02/10, 2022/02/18

- Proofs revision

### Ada-Paper on mortality (with L. Mangone)

#### 2021/10/04

- Discussion with A. Trama about the paper

#### 2021/10/19

- Call with the authors

#### 2021/10/25-2021/10/27

- Mortality tables definition

### Ada-SC

#### 2021/10/08

- SC preparation

#### 2021/10/11

- SC Call & minutes preparation

## ESSELUNGA-AYA project

#### 2021/10/14

- Tasks face-to-face discussion with A. Trama and L. Botta

## Miscellaneous

### Research department seminar

#### 2021/10/12-2021/10/13

- Oral presentation preparation for the Research Department (INT)

#### 2021/10/18

- Oral presentation at the Research Department (INT)

### Paper revisions

#### 2021/10/19-2021/10/22

- Frontiers paper revision

#### 2021/10/26

- NPC paper proofs revision

#### 2022/01/08-2022/01/08

- JAYAO paper revision


### DIANA WEB

#### 2021/11/02, 2021/11/30, 2021/12/09, 2021/12/15, 2022/01/13, 2022/01/27, 2022/02/09

- Meeting with A. Villarini

#### 2022/01/21, 2022/01/24-25, 2022/01/28, 2022/01/31, 2022/02/10

- Sample size calculation and discussion

### General planning

#### 2021/11/09, 2021/11/11, 2021/11/15

- Time-tables for 2022 preparation and discussion

### Seminars/Meetings/Workshop

#### 2021/11/16-2021/11/18

- ENCR 2021 online meeting

### Cancer Catcher

#### 2021/11/30, 2021/12/09, 2021/12/20, 2021/12/29

- Discussion with A Trama e P Lasalvia on project feasibility

#### 2021/11/21

- Call with all the partners involved


